from django.shortcuts import render


__all__ = ("denied",)


def denied(r):
    return render(r, "django_saml2_auth/denied.html")
