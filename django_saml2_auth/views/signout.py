from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from saml2 import BINDING_HTTP_POST, BINDING_HTTP_REDIRECT
from saml2.ident import decode

from ..client import get_saml_client
from ..utils import get_current_domain

__all__ = ("signout",)


def _get_subject_id(session):
    try:
        return decode(session["_saml2_subject_id"])
    except KeyError:
        return None


def _handle_logout_response(response):
    """
    Handles saml2 logout response.

    :param response: Saml2 logout response
    """
    if len(response) > 1:
        # Currently only one source is supported
        return HttpResponseServerError("Logout from several sources not supported")
    for entityid, logout_info in response.items():
        if isinstance(logout_info, tuple):
            # logout_info is a tuple containing header information and a HTML message.
            binding, http_info = logout_info
            if binding == BINDING_HTTP_POST:
                # Display content defined in logout response
                body = "".join(http_info["data"])
                return HttpResponse(body)
            elif binding == BINDING_HTTP_REDIRECT:
                # Redirect to address defined in logout response
                return HttpResponseRedirect(_get_location(http_info))
            else:
                # Unknown binding
                return HttpResponseServerError("Logout binding not supported")
        else:  # result from logout, should be OK
            pass

    return HttpResponseServerError("Failed to log out")


def _get_location(http_info):
    try:
        headers = dict(http_info["headers"])
        return headers["Location"]
    except KeyError:
        return http_info["url"]


def signout(r):
    """
    Initiates saml2 logout.

    :param r: HTTP request
    """
    binding = (
        settings.SAML2_AUTH.get("SAML_CLIENT_SETTINGS", {})
        .get("service", {})
        .get("sp", {})
        .get("binding", BINDING_HTTP_REDIRECT)
    )
    saml_client = get_saml_client(get_current_domain(r))
    subject_id = _get_subject_id(r.session)
    idp_entity_id = None
    try:
        idp_entity_id = settings.SAML2_AUTH["SAML_CLIENT_SETTINGS"]["service"]["sp"][
            "idp"
        ]
    except KeyError:
        return HttpResponseServerError("Idp not defined")

    response = saml_client.do_logout(
        subject_id, [idp_entity_id], "", None, expected_binding=binding
    )
    return _handle_logout_response(response)
