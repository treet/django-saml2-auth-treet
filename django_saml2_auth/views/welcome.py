from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import TemplateDoesNotExist

from ..utils import get_default_next_url


__all__ = ("welcome",)


def welcome(r):
    try:
        return render(r, "django_saml2_auth/welcome.html", {"user": r.user})
    except TemplateDoesNotExist:
        return HttpResponseRedirect(get_default_next_url())
