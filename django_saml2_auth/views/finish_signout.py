from django.contrib.auth import logout
from django.shortcuts import render

__all__ = ("finish_signout",)


def finish_signout(r):
    """
    Logout local user

    :param r: HTTP request
    """
    logout(r)
    return render(r, "django_saml2_auth/signout.html")
