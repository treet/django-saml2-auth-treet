from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.utils.http import is_safe_url
from saml2 import BINDING_HTTP_POST, BINDING_HTTP_REDIRECT

from ..client import get_saml_client
from ..utils import get_current_domain, get_default_next_url, get_reverse
from .denied import denied

__all__ = ("signin",)


def signin(r):
    try:
        import urlparse as _urlparse
        from urllib import unquote
    except:
        import urllib.parse as _urlparse
        from urllib.parse import unquote
    next_url = r.GET.get("next", get_default_next_url())

    try:
        if "next=" in unquote(next_url):
            next_url = _urlparse.parse_qs(_urlparse.urlparse(unquote(next_url)).query)[
                "next"
            ][0]
    except:
        next_url = r.GET.get("next", get_default_next_url())

    # Only permit signin requests where the next_url is a safe URL
    if not is_safe_url(next_url, None):
        return HttpResponseRedirect(
            get_reverse([denied, "denied", "django_saml2_auth:denied"])
        )

    r.session["login_next_url"] = next_url

    idp_entity_id = None
    try:
        idp_entity_id = settings.SAML2_AUTH["SAML_CLIENT_SETTINGS"]["service"]["sp"][
            "idp"
        ]
    except KeyError:
        pass

    relay_state = ""
    try:
        relay_state = settings.SAML2_AUTH["SAML_CLIENT_SETTINGS"]["service"]["sp"][
            "relay_state"
        ]
    except KeyError:
        pass

    binding = (
        settings.SAML2_AUTH.get("SAML_CLIENT_SETTINGS", {})
        .get("service", {})
        .get("sp", {})
        .get("binding", BINDING_HTTP_REDIRECT)
    )

    saml_client = get_saml_client(get_current_domain(r))
    _, info = saml_client.prepare_for_authenticate(
        idp_entity_id, relay_state=relay_state, binding=binding
    )

    if binding == BINDING_HTTP_REDIRECT:
        redirect_url = None
        for key, value in info["headers"]:
            if key == "Location":
                redirect_url = value
                break
        return HttpResponseRedirect(redirect_url)
    elif binding == BINDING_HTTP_POST:
        return HttpResponse(info["data"])
    else:
        return HttpResponseServerError("Sso binding not supported")
