from django import get_version
from django.conf import settings
from django.contrib.auth import get_user_model, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import TemplateDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from pkg_resources import parse_version
from saml2 import entity
from saml2.ident import code
from saml2.response import StatusAuthnFailed

if parse_version(get_version()) >= parse_version("1.7"):
    from django.utils.module_loading import import_string
else:
    from django.utils.module_loading import import_by_path as import_string

from ..client import get_saml_client
from ..utils import get_current_domain, get_default_next_url, get_reverse
from .denied import denied


__all__ = ("acs",)

user_model = get_user_model()


def _set_subject_id(session, subject_id):
    session["_saml2_subject_id"] = code(subject_id)


@login_required
def _create_new_user(user_identity):
    attributes_map = settings.SAML2_AUTH.get("ATTRIBUTES_MAP", {})
    user_name = user_identity[attributes_map.get("username", "UserName")][0]
    user = user_model.objects.create_user(user_name)
    for (user_attr, saml_attr) in attributes_map.items():
        if user_attr != "username":
            values = user_identity.get(saml_attr)
            if values is not None:
                setattr(user, user_attr, values[0])

    groups = [
        Group.objects.get(name=x)
        for x in settings.SAML2_AUTH.get("NEW_USER_PROFILE", {}).get("USER_GROUPS", [])
    ]
    if parse_version(get_version()) >= parse_version("2.0"):
        user.groups.set(groups)
    else:
        user.groups = groups
    user.is_active = settings.SAML2_AUTH.get("NEW_USER_PROFILE", {}).get(
        "ACTIVE_STATUS", True
    )
    user.is_staff = settings.SAML2_AUTH.get("NEW_USER_PROFILE", {}).get(
        "STAFF_STATUS", True
    )
    user.is_superuser = settings.SAML2_AUTH.get("NEW_USER_PROFILE", {}).get(
        "SUPERUSER_STATUS", False
    )
    if settings.SAML2_AUTH.get("TRIGGER", {}).get("NEW_USER", None):
        import_string(settings.SAML2_AUTH["TRIGGER"]["NEW_USER"])(user, user_identity)
    user.save()
    return user


@csrf_exempt
def acs(r):
    saml_client = get_saml_client(get_current_domain(r))
    resp = r.POST.get("SAMLResponse", None)
    next_url = r.session.get("login_next_url", get_default_next_url())

    if not resp:
        return HttpResponseRedirect(
            get_reverse([denied, "denied", "django_saml2_auth:denied"])
        )

    # Parsing the response throws `StatusAuthnFailed` exception on user related
    # authentication errors (such as user canceling the authentication), so
    # deny access if that happens.
    try:
        authn_response = saml_client.parse_authn_request_response(
            resp, entity.BINDING_HTTP_POST,
        )
    except StatusAuthnFailed:
        authn_response = None

    if authn_response is None:
        return HttpResponseRedirect(
            get_reverse([denied, "denied", "django_saml2_auth:denied"])
        )

    session_info = authn_response.session_info()
    user_identity = authn_response.get_identity()
    if user_identity is None:
        return HttpResponseRedirect(
            get_reverse([denied, "denied", "django_saml2_auth:denied"])
        )

    user_name = user_identity[
        settings.SAML2_AUTH.get("ATTRIBUTES_MAP", {}).get("username", "UserName")
    ][0]

    target_user = None
    is_new_user = False

    find_user_spec = settings.SAML2_AUTH.get("TRIGGER", {}).get("FIND_USER")
    if find_user_spec:
        find_user = import_string(find_user_spec)
        target_user = find_user(user_identity)
    else:
        target_user = user_model.objects.filter(username=user_name).first()

    if target_user:
        if settings.SAML2_AUTH.get("TRIGGER", {}).get("BEFORE_LOGIN", None):
            import_string(settings.SAML2_AUTH["TRIGGER"]["BEFORE_LOGIN"])(
                target_user, user_identity
            )
    else:
        target_user = _create_new_user(user_identity)
        if settings.SAML2_AUTH.get("TRIGGER", {}).get("CREATE_USER", None):
            import_string(settings.SAML2_AUTH["TRIGGER"]["CREATE_USER"])(
                target_user, user_identity
            )
        is_new_user = True

    r.session.flush()
    _set_subject_id(r.session, session_info["name_id"])

    if target_user.is_active:
        target_user.backend = "django.contrib.auth.backends.ModelBackend"
        login(r, target_user)
    else:
        return HttpResponseRedirect(
            get_reverse([denied, "denied", "django_saml2_auth:denied"])
        )

    if is_new_user:
        try:
            return render(r, "django_saml2_auth/welcome.html", {"user": r.user})
        except TemplateDoesNotExist:
            return HttpResponseRedirect(next_url)
    else:
        return HttpResponseRedirect(next_url)
