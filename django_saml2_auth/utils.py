from django import get_version
from django.conf import settings
from pkg_resources import parse_version

__all__ = ("get_current_domain", "get_default_next_url", "get_reverse")


def get_current_domain(r):
    if "ASSERTION_URL" in settings.SAML2_AUTH:
        return settings.SAML2_AUTH["ASSERTION_URL"]
    return "{scheme}://{host}".format(
        scheme="https" if r.is_secure() else "http", host=r.get_host(),
    )


def get_reverse(objs):
    """In order to support different django version, I have to do this """
    if parse_version(get_version()) >= parse_version("2.0"):
        from django.urls import reverse
    else:
        from django.core.urlresolvers import reverse
    if objs.__class__.__name__ not in ["list", "tuple"]:
        objs = [objs]

    for obj in objs:
        try:
            return reverse(obj)
        except:
            pass
    raise Exception(
        (
            "We got a URL reverse issue: %s. "
            "This is a known issue but please still submit a ticket at "
            "https://gitlab.com/treet/django-saml2-auth-treet/issues/new"
        )
        % str(objs)
    )


def get_default_next_url():
    configured_next_url = settings.SAML2_AUTH.get("DEFAULT_NEXT_URL")
    if configured_next_url:
        return configured_next_url
    else:
        return get_reverse("admin:index")
