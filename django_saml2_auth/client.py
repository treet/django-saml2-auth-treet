import collections

from django.conf import settings
from saml2 import BINDING_HTTP_POST, BINDING_HTTP_REDIRECT
from saml2.client import Saml2Client
from saml2.config import Config as Saml2Config

from .utils import get_reverse

__all__ = ("get_saml_client",)


def _merge_dict(d1, d2):
    for k, v2 in d2.items():
        v1 = d1.get(k)
        if isinstance(v1, collections.Mapping) and isinstance(v2, collections.Mapping):
            _merge_dict(v1, v2)
        else:
            d1[k] = v2


def get_saml_client(domain):
    acs_url = domain + get_reverse(["acs", "django_saml2_auth:acs"])

    saml_settings = {
        "service": {
            "sp": {
                "endpoints": {
                    "assertion_consumer_service": [
                        (acs_url, BINDING_HTTP_REDIRECT),
                        (acs_url, BINDING_HTTP_POST),
                    ],
                },
                "allow_unsolicited": True,
                "authn_requests_signed": False,
                "logout_requests_signed": True,
                "want_assertions_signed": True,
                "want_response_signed": False,
            },
        },
    }

    _merge_dict(saml_settings, settings.SAML2_AUTH["SAML_CLIENT_SETTINGS"])

    spConfig = Saml2Config()
    spConfig.load(saml_settings)
    spConfig.allow_unknown_attributes = True
    saml_client = Saml2Client(config=spConfig)
    return saml_client
